import {
  CONFIRM_SELECT_SEAT,
  SELECTED_SEAT,
  STARTING_SELECT_SEAT,
} from "../contants/ActionTypes";
import danhSachGheData from "../danhSachGheData";

let initialState = {
  isStartSelect: false,
  isSubmited: false,
  seatList: danhSachGheData,
  name: "",
  numberOfSeats: 0,
  selectedSeats: [],
};

export const bookTickerReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SELECTED_SEAT:
      if (
        state.numberOfSeats > 0 &&
        state.selectedSeats.length < state.numberOfSeats &&
        state.selectedSeats.findIndex((x) => x.soGhe === payload.soGhe) === -1
      ) {
        return {
          ...state,
          selectedSeats: [...state.selectedSeats, payload],
        };
      }

      return state;
    case STARTING_SELECT_SEAT:
      return {
        ...state,
        isStartSelect: true,
        name: payload.name,
        numberOfSeats: payload.numberOfSeats,
      };
    case CONFIRM_SELECT_SEAT:
      return {
        ...state,
        isSubmited: true,
      };
    default:
      return state;
  }
};
