import { combineReducers } from "redux";
import { bookTickerReducer } from "./bookTickerReducer";

export const rootReducer = combineReducers({
  ticker: bookTickerReducer,
});
