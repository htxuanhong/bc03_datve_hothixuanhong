import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { STARTING_SELECT_SEAT } from "../contants/ActionTypes";
class DatGheForm extends PureComponent {
  state = {
    name: "",
    numberOfSeats: 0,
  };

  render() {
    const { isStartSelect } = this.props;

    return (
      <form
        onSubmit={(event) => {
          event.preventDefault();

          var formData = new FormData(event.target);

          const value = Object.fromEntries(formData.entries());

          this.props.handlestartSelectSeat(value);
        }}
      >
        <div className="item-form">
          <div className="item-left">
            <label htmlFor="name">
              Name <span>*</span>
            </label>
            <input
              type="text"
              id="name"
              name="name"
              className="form-control"
              aria-describedby="helpId"
              disabled={isStartSelect}
            />
          </div>

          <div className="item-right">
            <label htmlFor="seat">
              Number of Seats <span>*</span>
            </label>
            <input
              type="number"
              id="seat"
              name="numberOfSeats"
              className="form-control"
              aria-describedby="helpId"
              disabled={isStartSelect}
            />
          </div>
        </div>
        <div>
          <button type="submit" className="btn btn-light button-start">
            Start Selecting
          </button>
        </div>

        <ul>
          <li className="contentSmall greenSeat">Selected Seat</li>
          <li className="contentSmall redSeat">Reserved Seat</li>
          <li className="contentSmall emptySeat">Empty Seat</li>
        </ul>

        {isStartSelect && (
          <div className="select-ple">Please Select your Seats NOW!</div>
        )}
      </form>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    isStartSelect: state.ticker.isStartSelect,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handlestartSelectSeat: (payload) => {
      dispatch({
        type: STARTING_SELECT_SEAT,
        payload,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DatGheForm);
