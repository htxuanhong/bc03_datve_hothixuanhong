import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { CONFIRM_SELECT_SEAT } from "../contants/ActionTypes";

class Screen extends PureComponent {
  render() {
    const {
      isSubmited1,
      name1,
      numberOfSeats1,
      selectedSeats1,
      handleExportForm,
    } = this.props;

    return (
      <div className="text-center screen-way ">
        <h5>SCREEN THIS WAY</h5>
        <button
          className="btn btn-light button-start mb-4"
          onClick={handleExportForm}
        >
          Confirm Selection
        </button>

        <table className="table table-bordered">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Number of Seats</th>
              <th scope="col">Seats</th>
            </tr>
          </thead>
          <tbody>
            {isSubmited1 ? (
              <>
                <tr>
                  <td>{name1}</td>
                  <td>{numberOfSeats1}</td>
                  <td>{selectedSeats1.map((x) => x.soGhe).join(", ")}</td>
                </tr>
              </>
            ) : (
              <>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
              </>
            )}
          </tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    isSubmited1: state.ticker.isSubmited,
    name1: state.ticker.name,
    numberOfSeats1: state.ticker.numberOfSeats,
    selectedSeats1: state.ticker.selectedSeats,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleExportForm: () => {
      dispatch({
        type: CONFIRM_SELECT_SEAT,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Screen);
