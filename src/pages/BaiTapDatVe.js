import React, { Component } from "react";
import { connect } from "react-redux";
import DatGheForm from "./DatGheForm";
import Screen from "./Screen";
import SeatList from "./SeatList";

class BaiTapDatVe extends Component {
  render() {
    const { selectedSeats, seatList, selectedSeat } = this.props;

    return (
      <div>
        <h1 className="title text-center text-white my-5">
          MOVIE SEAT SELECTION
        </h1>
        <div className="container ">
          <div className="form-seat">
            <p>Fill The Required Details Below And Select Your Seats</p>

            <DatGheForm />

            <div className="hint-group">
              <div className="gheDuocChon"></div>
              <div className="gheDangChon"></div>
            </div>

            <SeatList
              selectedSeats={selectedSeats}
              seatList={seatList}
              selectedSeat={selectedSeat}
            />

            <Screen />
          </div>
        </div>
        <div className="copyright">
          <p>
            © 2018 Movie Seat Selection . All Rights Reserved | Design by
            W3layouts
          </p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    seatList: state.ticker.seatList,
    selectedSeats: state.ticker.selectedSeats,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    selectedSeat: (payload) =>
      dispatch({
        type: "SELECTED_SEAT",
        payload,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BaiTapDatVe);
