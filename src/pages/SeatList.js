import React, { PureComponent } from "react";

class SeatList extends PureComponent {
  render() {
    const { selectedSeats, seatList, selectedSeat } = this.props;

    return (
      <div className="seat-list-container text-white">
        {seatList.map((row, rowIndex) => {
          return (
            <React.Fragment key={rowIndex}>
              {rowIndex === 5 && <div className="row-space"></div>}

              <div className="row" key={rowIndex}>
                <div className="cell label">{row.hang}</div>

                {row.danhSachGhe.map((ghe, cellIndex) => {
                  return (
                    <React.Fragment key={cellIndex}>
                      {cellIndex === 5 && <div className="column-space"></div>}

                      {row.hang === "" ? (
                        <div className="cell label">{ghe.label}</div>
                      ) : (
                        <GheItem
                          value={ghe}
                          isSelected={
                            selectedSeats.findIndex(
                              (x) => x.soGhe === ghe.soGhe
                            ) > -1
                          }
                          onClick={(v) => {
                            console.log("dang chon ghe", v);
                            selectedSeat(ghe);
                          }}
                        />
                      )}
                    </React.Fragment>
                  );
                })}
              </div>
            </React.Fragment>
          );
        })}
      </div>
    );
  }
}

class GheItem extends PureComponent {
  render() {
    const { isSelected, value, onClick } = this.props;

    let className = "seat-cell ghe";
    if (value.daDat) {
      className += " gheDuocChon";
    }

    if (isSelected) {
      className += " gheDangChon";
    }

    return (
      <div
        className={className}
        onClick={() => !value.daDat && onClick(value)}
      ></div>
    );
  }
}

export default SeatList;
